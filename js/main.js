let sInputNum = "";
let nInputNum;
let sFounds = "";

lbMultiplesOf5: {
    do {
        sInputNum = prompt("Searching for multiples of 5. Enter a positive integer number", sInputNum);
        if (sInputNum === null) break lbMultiplesOf5;
        nInputNum = +sInputNum;
    } while (Number.isNaN(nInputNum) || !Number.isInteger(nInputNum) || nInputNum < 1);

    console.log(`Searching for multiples of 5 up to ${nInputNum} ..`);

    if (nInputNum < 5)
        console.log(`Sorry, no numbers found`);

    for (let i = 5; i <= nInputNum; i += 5)
        console.log(`${i}`);

    console.log(`Searching complete`);
}



let sFrom = "";
let sTo = "";
let nFrom;
let nTo;

lbPrimes: {
    do {
        sFrom = prompt(`Searching for primes. Enter the first positive integer number`, sFrom);
        if (sFrom === null) break lbPrimes;
        nFrom = +sFrom;
    } while (Number.isNaN(nFrom) || !Number.isInteger(nFrom) || nFrom < 1);

    do {
        sTo = prompt(`Enter the second integer number that is greater then ${nFrom}`, sTo);
        if (sTo === null) break lbPrimes;
        nTo = +sTo;
    } while (Number.isNaN(nTo) || !Number.isInteger(nTo) || nTo <= nFrom);

    console.log(`Searching for primes from ${nFrom} to ${nTo} ..`);

    if (nFrom < 3) console.log(`2`);

    for (let nCurrent = Math.max(3, nFrom); nCurrent <= nTo; nCurrent++) {
        let bDivFound = (nCurrent % 2 === 0);
        for (let j = 3; !bDivFound && j <= Math.sqrt(nCurrent); j += 2)
            bDivFound = (nCurrent % j === 0);
        if (!bDivFound) console.log(`${nCurrent}`);
    }

    console.log(`Searching complete`);
}